import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Interfaz extends JFrame{
    private JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JButton btnConectar;
    private JButton btnVisualizar;
    private JButton btnDescargar;
    private JList list;

    Interfaz(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 513, 476);
        contentPane = new JPanel();
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(10, 11, 477, 415);
        contentPane.add(panel);
        panel.setLayout(null);

        JLabel lblDireccinParaConectar = new JLabel("Direcci\u00F3n para conectar");
        lblDireccinParaConectar.setBounds(10, 11, 121, 14);
        panel.add(lblDireccinParaConectar);

        textField = new JTextField("ftp.rediris.es");
        textField.setBounds(141, 8, 142, 20);
        panel.add(textField);
        textField.setColumns(10);

        JLabel lblUsuario = new JLabel("Usuario");
        lblUsuario.setBounds(10, 48, 56, 14);
        panel.add(lblUsuario);

        textField_1 = new JTextField("anonymous");
        textField_1.setBounds(92, 45, 102, 20);
        panel.add(textField_1);
        textField_1.setColumns(10);

        JLabel lblContrasea = new JLabel("Contrase\u00F1a");
        lblContrasea.setBounds(10, 83, 71, 14);
        panel.add(lblContrasea);

        textField_2 = new JTextField("anonymous");
        textField_2.setBounds(90, 80, 104, 20);
        panel.add(textField_2);
        textField_2.setColumns(10);

        btnConectar = new JButton("Conectar");
        btnConectar.setBounds(313, 7, 89, 23);
        panel.add(btnConectar);


        list = new JList();
        JScrollPane scrollPane=new JScrollPane(list);
        scrollPane.setBounds(10, 157, 457, 183);
        panel.add(scrollPane);

        JLabel lblRutaDelArchivo = new JLabel("Nombre del archivo a descargar");
        lblRutaDelArchivo.setBounds(10, 351, 168, 14);
        panel.add(lblRutaDelArchivo);

        textField_3 = new JTextField();
        textField_3.setBounds(174, 348, 293, 20);
        panel.add(textField_3);
        textField_3.setColumns(10);

        btnDescargar = new JButton("Descargar");
        btnDescargar.setBounds(10, 381, 89, 23);
        panel.add(btnDescargar);

        JLabel lblRutaParaVisualizar = new JLabel("Ruta para visualizar");
        lblRutaParaVisualizar.setBounds(10, 132, 102, 14);
        panel.add(lblRutaParaVisualizar);

        textField_4 = new JTextField();
        textField_4.setBounds(122, 129, 207, 20);
        panel.add(textField_4);
        textField_4.setColumns(10);

        btnVisualizar = new JButton("Visualizar");
        btnVisualizar.setBounds(341, 128, 89, 23);
        panel.add(btnVisualizar);
    }
}



class StartInterface{
    public static void main(String[] args) {
        Interfaz interfaz=new Interfaz();
        interfaz.setVisible(true);
    }
}
