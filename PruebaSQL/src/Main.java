
import objetosMapeados.DepartamentosEntity;
import objetosMapeados.EmpleadosEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Crear el dato departamento, osea crear un departamento
        Date fecha=new Date(2019, 05, 22);
        HibernateUtil.CrearSessionFactory(); //Se crea la sesión
        DepartamentosEntity departamentosAutentico=new DepartamentosEntity();
        departamentosAutentico.setDeptno(55);
        departamentosAutentico.setNombreDeptno("Inno");
        departamentosAutentico.setLocalidad("Aguimes");

        List<EmpleadosEntity> empleadosAutenticos=new ArrayList<>();

        //Creo el empleado nº1
        EmpleadosEntity empleadosEntity1=new EmpleadosEntity();
        empleadosEntity1.setEmpno(551);
        empleadosEntity1.setNombreEmpno("Yasmina");
        empleadosEntity1.setCargo("Becario");
        empleadosEntity1.setFehcaIng(fecha);
        empleadosEntity1.setSalario(4000);
        empleadosEntity1.setComision(200);
        empleadosEntity1.setDeptnoList(departamentosAutentico);

        //Creo el empleado nº2
        EmpleadosEntity empleadosEntity2=new EmpleadosEntity();
        empleadosEntity2.setEmpno(552);
        empleadosEntity2.setNombreEmpno("Juan");
        empleadosEntity2.setCargo("Ingeniero");
        empleadosEntity2.setFehcaIng(fecha);
        empleadosEntity2.setSalario(2000);
        empleadosEntity2.setComision(100);
        empleadosEntity2.setDeptnoList(departamentosAutentico);

        //Creo el empleado nº3
        EmpleadosEntity empleadosEntity3=new EmpleadosEntity();
        empleadosEntity3.setEmpno(553);
        empleadosEntity3.setNombreEmpno("Rita");
        empleadosEntity3.setCargo("Programadora");
        empleadosEntity3.setFehcaIng(fecha);
        empleadosEntity3.setSalario(1500);
        empleadosEntity3.setComision(100);
        empleadosEntity3.setDeptnoList(departamentosAutentico);

        //Aqui agrego los empleados a la lista, para decir que pertenecen a dicho departamento
        empleadosAutenticos.add(empleadosEntity1);
        empleadosAutenticos.add(empleadosEntity2);
        empleadosAutenticos.add(empleadosEntity3);

        departamentosAutentico.setEmpleList(empleadosAutenticos);


        Session session= HibernateUtil.getEstadoSession(); //Creo la sesión
        session.beginTransaction(); //Inicio la transacción
        session.save(departamentosAutentico);   //Guardo el departamento
        session.save(empleadosEntity1);         //Guardo el empleado nº1
        session.save(empleadosEntity2);         //Guardo el empleado nº2
        session.save(empleadosEntity3);         //Guardo el empleado nº3
        session.getTransaction().commit();      //Finalizo la transacción

        session.close();                        //Cierro la sesión


        /*Creamos una nueva sesión para ejecutar una sentencia*/
        Session session1=HibernateUtil.getEstadoSession();
        /*Creamos la sentencia en lenguaje HQL*/
        Query query= session1.createQuery("from EmpleadosEntity pp where pp.empno = :empno");
        query.setParameter("empno", 551); //Aqui le asignamos un valor al paramentro
        ArrayList<EmpleadosEntity> empleadosEntities=(ArrayList<EmpleadosEntity>) query.list(); //Creo la lista para guardar lo que devolverá la sentencia

        /*Utilizo un bucle foreach para recorrer la lista y mostrar su contenido*/
        for (EmpleadosEntity empleadosEntity: empleadosEntities) {
            System.out.println(empleadosEntity.getEmpno()+" "+empleadosEntity.getNombreEmpno()+" "+empleadosEntity.getFehcaIng()+" "+empleadosEntity.getCargo()+" "+empleadosEntity.getSalario()+
                    " "+empleadosEntity.getComision()+" "+empleadosEntity.getDeptnoList());
        }
        session1.close(); //Aqui cierro la sesión

        Session session2=HibernateUtil.getEstadoSession();

        Query query2= session2.createQuery("from EmpleadosEntity"); //Selecciona todos los empleados
        ArrayList<EmpleadosEntity> empleadosEntities2=(ArrayList<EmpleadosEntity>) query2.list();

        for (EmpleadosEntity empleadosEntity: empleadosEntities2) {
            System.out.println(empleadosEntity.getEmpno()+" "+empleadosEntity.getNombreEmpno()+" "+empleadosEntity.getFehcaIng()+" "+empleadosEntity.getCargo()+" "+empleadosEntity.getSalario()+
                    " "+empleadosEntity.getComision()+" "+empleadosEntity.getDeptnoList());
        }
        session2.close();


        //Para borrar el empleado
        Session sesion3 = HibernateUtil.getEstadoSession(); //Creo una nueva sesión

        /*Ejecuto la sentencia para que me dé todos los datos del empleado*/
        Query query3= sesion3.createQuery("from EmpleadosEntity pp where pp.empno = :empno");
        query3.setParameter("empno", 553); //Utilizo la asignación por paramentros
        EmpleadosEntity empleadosEntity=(EmpleadosEntity)query3.uniqueResult(); //Creo un nuevo objeto que contendrá los datos que me devuelve la consulta
        sesion3.beginTransaction();
        sesion3.delete(empleadosEntity); //Borro el empleado
        sesion3.getTransaction().commit();
        sesion3.close();


        //Modificar sus propiedades
        Session sesion4 = HibernateUtil.getEstadoSession();

        Query query4= sesion4.createQuery("from EmpleadosEntity pp where pp.empno = :empno");
        query4.setParameter("empno", 552);
        EmpleadosEntity empleadosEntity4=(EmpleadosEntity)query4.uniqueResult();//Creo un nuevo objeto que contendrá los datos que me devuelve la consulta
        empleadosEntity4.setSalario(empleadosEntity4.getSalario()+500);//Aqui utilizo los metodos para modificar el valor que me ha devuelto
        sesion4.beginTransaction();
        sesion4.save(empleadosEntity4); //Guardo dicho objeto
        sesion4.getTransaction().commit(); //Guardo en la base de datos
        sesion4.close();

        HibernateUtil.CerrarSessionFactory(); //Cerrar la conexión
    }
}

/*EmpleadosEntity empleadosAutentico=new EmpleadosEntity();
        empleadosAutentico.setEmpno(551);
        empleadosAutentico.setNombreEmpno("Yasmina");
        empleadosAutentico.setCargo("Becario");
        empleadosAutentico.setFehcaIng(fecha);
        empleadosAutentico.setSalario(4000);
        empleadosAutentico.setComision(200);
        empleadosAutentico.setDeptnoList(departamentosAutentico);*/