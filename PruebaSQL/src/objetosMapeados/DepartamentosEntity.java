package objetosMapeados;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "departamentos", schema = "empleados_departamentos", catalog = "")
public class DepartamentosEntity {
    private int deptno;
    private String nombreDeptno;
    private String localidad;
    private List<EmpleadosEntity> empleList;

    @Id
    @Column(name = "deptno")
    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }

    @Basic
    @Column(name = "nombreDeptno")
    public String getNombreDeptno() {
        return nombreDeptno;
    }

    public void setNombreDeptno(String nombreDeptno) {
        this.nombreDeptno = nombreDeptno;
    }

    @Basic
    @Column(name = "localidad")
    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartamentosEntity that = (DepartamentosEntity) o;
        return deptno == that.deptno &&
                Objects.equals(nombreDeptno, that.nombreDeptno) &&
                Objects.equals(localidad, that.localidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptno, nombreDeptno, localidad);
    }

    @OneToMany(mappedBy = "deptnoList")
    public List<EmpleadosEntity> getEmpleList() {
        return empleList;
    }

    public void setEmpleList(List<EmpleadosEntity> empleList) {
        this.empleList = empleList;
    }
}
