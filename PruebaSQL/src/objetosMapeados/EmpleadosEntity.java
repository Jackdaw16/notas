package objetosMapeados;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "empleados", schema = "empleados_departamentos")
public class EmpleadosEntity {
    private int empno;
    private String nombreEmpno;
    private String cargo;
    private Date fehcaIng;
    private Integer salario;
    private Integer comision;
    private DepartamentosEntity deptnoList;


    /*public EmpleadosEntity(int empno, String nombreEmpno, String cargo, Date fehcaIng, Integer salario, Integer comision, DepartamentosEntity deptnoList) {
        this.empno = empno;
        this.nombreEmpno = nombreEmpno;
        this.cargo = cargo;
        this.fehcaIng = fehcaIng;
        this.salario = salario;
        this.comision = comision;
        this.deptnoList = deptnoList;
    }*/


    @Id
    @Column(name = "empno")
    public int getEmpno() {
        return empno;
    }

    public void setEmpno(int empno) {
        this.empno = empno;
    }

    @Basic
    @Column(name = "nombreEmpno")
    public String getNombreEmpno() {
        return nombreEmpno;
    }

    public void setNombreEmpno(String nombreEmpno) {
        this.nombreEmpno = nombreEmpno;
    }

    @Basic
    @Column(name = "cargo")
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Basic
    @Column(name = "fehcaIng")
    public Date getFehcaIng() {
        return fehcaIng;
    }

    public void setFehcaIng(Date fehcaIng) {
        this.fehcaIng = fehcaIng;
    }

    @Basic
    @Column(name = "salario")
    public Integer getSalario() {
        return salario;
    }

    public void setSalario(Integer salario) {
        this.salario = salario;
    }

    @Basic
    @Column(name = "comision")
    public Integer getComision() {
        return comision;
    }

    public void setComision(Integer comision) {
        this.comision = comision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpleadosEntity that = (EmpleadosEntity) o;
        return empno == that.empno &&
                Objects.equals(nombreEmpno, that.nombreEmpno) &&
                Objects.equals(cargo, that.cargo) &&
                Objects.equals(fehcaIng, that.fehcaIng) &&
                Objects.equals(salario, that.salario) &&
                Objects.equals(comision, that.comision);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empno, nombreEmpno, cargo, fehcaIng, salario, comision);
    }

    @ManyToOne
    @JoinColumn(name = "deptno", referencedColumnName = "deptno")
    public DepartamentosEntity getDeptnoList() {
        return deptnoList;
    }

    public void setDeptnoList(DepartamentosEntity deptnoList) {
        this.deptnoList = deptnoList;
    }
}
