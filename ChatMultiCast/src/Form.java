import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Form extends JFrame{

    public Form(){
        setBounds(0, 0, 450, 350);
        LaminaMarco marco=new LaminaMarco();
        add(marco);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
}

class LaminaMarco extends JPanel{
    
    final String direccion="225.1.1.1";
    final int puerto=6999;
    private MulticastSocket ms;
    private InetAddress grupo;
    private JTextField campoName;
    private JTextField campoMensaje;
    private TextArea campoRecibido;
    private JButton mineSend;


    public LaminaMarco(){

        JLabel lblName=new JLabel("Tu nombre: ");
        add(lblName);
        campoName=new JTextField(15);
        add(campoName);

        JLabel lblMensaje=new JLabel("Mensaje: ");
        add(lblMensaje);
        campoMensaje=new JTextField(15);
        add(campoMensaje);

        mineSend=new JButton("Enviar");
        add(mineSend);

        campoRecibido=new TextArea();
        add(campoRecibido);

    }
}






