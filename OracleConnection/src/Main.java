import oracle.Departamentos;
import oracle.Empleados;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        Date fecha=new Date(2019, 05, 66);
        HibernateUtil.CrearSessionFactory();
        Departamentos departamento=new Departamentos();
        departamento.setDeptno(55);
        departamento.setNombredeptno("Innov");
        departamento.setLocalidad("Aguimes");

        Empleados empleado=new Empleados();
        empleado.setEmpno(551);
        empleado.setNombreempno("Yasmina");
        empleado.setCargo("Beca");
        empleado.setFechaing(new Time(100));
        empleado.setSalario((long)4000);
        empleado.setComision((long)200);
        empleado.setDepartamento(departamento);

        Empleados empleados=new Empleados();
        empleados.setEmpno(552);
        empleados.setNombreempno("Juan");
        empleados.setCargo("Ingen");
        empleados.setFechaing(new Time(100));
        empleados.setSalario((long)2000);
        empleados.setComision((long)100);
        empleados.setDepartamento(departamento);

        //Creo el empleado nº3
        Empleados empleados1=new Empleados();
        empleados1.setEmpno(553);
        empleados1.setNombreempno("Rita");
        empleados1.setCargo("Programa");
        empleados1.setFechaing(new Time(100));
        empleados1.setSalario((long)1500);
        empleados1.setComision((long)100);
        empleados1.setDepartamento(departamento);

        System.out.println("ANTES DE GUARDAR");
        Session session= HibernateUtil.getEstadoSession();
        session.beginTransaction();
        session.save(departamento);

        session.persist(empleado);  //Necesario para el tema de las relaciones
        session.merge(empleado); //Esto tambien

        session.persist(empleados);
        session.merge(empleados);

        session.persist(empleados1);
        session.merge(empleados1);

        System.out.println("BEFORE COMMIT");
        session.getTransaction().commit();
        System.out.println("AFTER COMMIT");

        session.close();

        Session session1=HibernateUtil.getEstadoSession();

        Query query= session1.createQuery("from Empleados pp where pp.empno = :empno");
        query.setParameter("empno", (long)551);
        ArrayList<Empleados> empleadosEntities=(ArrayList<Empleados>) query.list();

        for (Empleados empleadosEntity: empleadosEntities) {
            System.out.println(empleadosEntity.getEmpno()+" "+empleadosEntity.getNombreempno()+" "+empleadosEntity.getFechaing()+" "+empleadosEntity.getCargo()+" "+empleadosEntity.getSalario()+
                    " "+empleadosEntity.getComision()+" "+empleadosEntity.getDepartamento());
        }

        session1.close();

        Session session2=HibernateUtil.getEstadoSession();

        Query query2= session2.createQuery("from Empleados"); //Selecciona todos los empleados
        ArrayList<Empleados> empleadosEntities2=(ArrayList<Empleados>) query2.list();

        for (Empleados empleadosEntity: empleadosEntities2) {
            System.out.println(empleadosEntity.getEmpno()+" "+empleadosEntity.getNombreempno()+" "+empleadosEntity.getFechaing()+" "+empleadosEntity.getCargo()+" "+empleadosEntity.getSalario()+
                    " "+empleadosEntity.getComision()+" "+empleadosEntity.getDepartamento());
        }
        session2.close();

        Session sesion3 = HibernateUtil.getEstadoSession(); //Creo una nueva sesión

        /*Ejecuto la sentencia para que me dé todos los datos del empleado*/
        Query query3= sesion3.createQuery("from Empleados pp where pp.empno = :empno");
        query3.setParameter("empno", (long)553);
        Empleados empleadosEntity=(Empleados)query3.uniqueResult();
        sesion3.beginTransaction();
        sesion3.delete(empleadosEntity);
        sesion3.getTransaction().commit();
        sesion3.close();


        Session sesion4 = HibernateUtil.getEstadoSession();

        Query query4= sesion4.createQuery("from Empleados pp where pp.empno = :empno");
        query4.setParameter("empno", (long)552);
        Empleados empleadosEntity4=(Empleados)query4.uniqueResult();
        empleadosEntity4.setSalario(empleadosEntity4.getSalario()+500);
        sesion4.beginTransaction();
        sesion4.save(empleadosEntity4);
        sesion4.getTransaction().commit();
        sesion4.close();

        //Ejercicio 6

        ConsultaGenerica();
        EntradaPorParamentros();
        EliminarEmple();
        ConsultaTablasRelacionadas();

        HibernateUtil.CerrarSessionFactory();




    }

    private static void ConsultaTablasRelacionadas() {
        Session session = HibernateUtil.getEstadoSession();
        Transaction t = session.beginTransaction();
        Query queryShow = session.createQuery("from Empleados e, Departamentos d where"
                + " e.departamento.deptno = d.deptno");
        Iterator itemp = queryShow.iterate();
        while (itemp.hasNext()) {
            Object[] reg = (Object[])itemp.next();
            Empleados emp = (Empleados)reg[0];
            Departamentos dep = (Departamentos)reg[1];
            System.out.println("Nombre: " + emp.getNombreempno());
            System.out.println("Salario: " + emp.getSalario());
            System.out.println("Departamento: " + dep.getDeptno());
            System.out.println("Nombre departamento: " + dep.getNombredeptno());

        }

        t.commit();
        session.close();
    }

    private static void EliminarEmple() {
        Session session=HibernateUtil.getEstadoSession();
        Query queryDelete=session.createQuery("from Empleados em where em.departamento.localidad like 'Aguimes'");
        Iterator itemp=queryDelete.iterate();
        session.beginTransaction();
        while (itemp.hasNext()){
            Empleados empleados=(Empleados) itemp.next();
            session.delete(empleados);
        }
        session.getTransaction().commit();
        session.close();

    }

    private static void ConsultaGenerica(){
        System.out.println("Introduce el nombre del primer departamento");
        String departamento1=scanner.src().nextLine();
        System.out.println("Introduce el nombre del segundo departamento");
        String departamento2=scanner.src().nextLine();

        Session session=HibernateUtil.getEstadoSession();
        Query queryShow=session.createQuery("from Empleados em where ((em.salario > 1500) AND (( em.departamento.nombredeptno like :departamento1 ) or (em.departamento.nombredeptno like :departamento2)))");
        queryShow.setParameter("departamento1", departamento1);
        queryShow.setParameter("departamento2", departamento2);
        Iterator itemp=queryShow.iterate();
        while (itemp.hasNext()){
            Empleados empleados=(Empleados) itemp.next();
            System.out.println(empleados.getEmpno()+" - "+empleados.getNombreempno()+" - "+empleados.getCargo()+" - "+empleados.getFechaing()+" - "+empleados.getSalario());
        }
        session.close();


    }

    private static void EntradaPorParamentros() {
        System.out.println("Introduce el nombre del departamento al cual quieres aumentar el sueldo");
        String departamento = scanner.src().nextLine();

        Session sesion = HibernateUtil.getEstadoSession();
        Query queryMod = sesion.createQuery("update Empleados em set em.salario = em.salario + 500 where ((em.departamento in (SELECT de.deptno FROM Departamentos de WHERE (de.nombredeptno LIKE :nombre))))");
        queryMod.setParameter("nombre", departamento);
        sesion.beginTransaction();
        queryMod.executeUpdate();
        sesion.getTransaction().commit();

        sesion.close();

        Session session2=HibernateUtil.getEstadoSession();

        Query query2= session2.createQuery("from Empleados"); //Selecciona todos los empleados
        ArrayList<Empleados> empleadosEntities2=(ArrayList<Empleados>) query2.list();

        for (Empleados empleadosEntity: empleadosEntities2) {
            System.out.println(empleadosEntity.getEmpno()+" "+empleadosEntity.getNombreempno()+" "+empleadosEntity.getFechaing()+" "+empleadosEntity.getCargo()+" "+empleadosEntity.getSalario()+
                    " "+empleadosEntity.getComision()+" "+empleadosEntity.getDepartamento());
        }
        session2.close();

    }
}