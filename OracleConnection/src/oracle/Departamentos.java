package oracle;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Departamentos {
    private long deptno;
    private String nombredeptno;
    private String localidad;
    private Collection<Empleados> empleados;

    @Id
    @Column(name = "DEPTNO")
    public long getDeptno() {
        return deptno;
    }

    public void setDeptno(long deptno) {
        this.deptno = deptno;
    }

    @Basic
    @Column(name = "NOMBREDEPTNO")
    public String getNombredeptno() {
        return nombredeptno;
    }

    public void setNombredeptno(String nombredeptno) {
        this.nombredeptno = nombredeptno;
    }

    @Basic
    @Column(name = "LOCALIDAD")
    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Departamentos that = (Departamentos) o;
        return deptno == that.deptno &&
                Objects.equals(nombredeptno, that.nombredeptno) &&
                Objects.equals(localidad, that.localidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptno, nombredeptno, localidad);
    }

    @OneToMany(mappedBy = "departamento")
    public Collection<Empleados> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Collection<Empleados> empleados) {
        this.empleados = empleados;
    }
}
