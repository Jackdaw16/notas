package oracle;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
public class Empleados {
    private long empno;
    private String nombreempno;
    private String cargo;
    private Time fechaing;
    private Long salario;
    private Long comision;
    private Departamentos departamento;

    @Id
    @Column(name = "EMPNO")
    public long getEmpno() {
        return empno;
    }

    public void setEmpno(long empno) {
        this.empno = empno;
    }

    @Basic
    @Column(name = "NOMBREEMPNO")
    public String getNombreempno() {
        return nombreempno;
    }

    public void setNombreempno(String nombreempno) {
        this.nombreempno = nombreempno;
    }

    @Basic
    @Column(name = "CARGO")
    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Basic
    @Column(name = "FECHAING")
    public Time getFechaing() {
        return fechaing;
    }

    public void setFechaing(Time fechaing) {
        this.fechaing = fechaing;
    }

    @Basic
    @Column(name = "SALARIO")
    public Long getSalario() {
        return salario;
    }

    public void setSalario(Long salario) {
        this.salario = salario;
    }

    @Basic
    @Column(name = "COMISION")
    public Long getComision() {
        return comision;
    }

    public void setComision(Long comision) {
        this.comision = comision;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleados empleados = (Empleados) o;
        return empno == empleados.empno &&
                Objects.equals(nombreempno, empleados.nombreempno) &&
                Objects.equals(cargo, empleados.cargo) &&
                Objects.equals(fechaing, empleados.fechaing) &&
                Objects.equals(salario, empleados.salario) &&
                Objects.equals(comision, empleados.comision);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empno, nombreempno, cargo, fechaing, salario, comision);
    }

    @ManyToOne
    @JoinColumn(name = "DEPTNO", referencedColumnName = "DEPTNO")
    public Departamentos getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamentos departamento) {
        this.departamento = departamento;
    }
}
