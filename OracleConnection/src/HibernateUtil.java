import oracle.Departamentos;
import oracle.Empleados;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

    private static SessionFactory sessionFactory;
    private static Session session;

    public static void CrearSessionFactory(){
        Configuration configuration = new Configuration();
        configuration.configure()
        .setProperty("hibernate.connection.username", "system")
        .setProperty("hibernate.connection.password", "elcancer_16");
        // Se registran las clases que hay que mapear con cada tabla de la base de datos
        configuration.addAnnotatedClass(Departamentos.class);
        configuration.addAnnotatedClass(Empleados.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    /**
     * Abre una nueva sesión
     */
    public static void AbrirSession() {
        session = sessionFactory.openSession();
    }

    /**
     * Devuelve la sesión actual
     * @return
     */
    public static Session getEstadoSession() {

        if ((session == null) || (!session.isOpen()))
            AbrirSession();

        return session;
    }

    /**
     * Cierra Hibernate
     */
    public static void CerrarSessionFactory() {

        if (session != null)
            session.close();

        if (sessionFactory != null)
            sessionFactory.close();
    }
}

