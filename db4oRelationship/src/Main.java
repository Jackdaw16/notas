import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;


public class Main {
    public static void main(String[] args) {
        //Conexiones a las bases de datos
        ObjectContainer dbDepartment = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), "dbDepartament.yap");
        ObjectContainer dbEmployed = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), "dbEmployed.yap");

        //Insertar valores que tambien comprueban las PK
        db4Connection(dbDepartment, dbEmployed);
        //Muestra todos los departamentos
        showAllDeparments(dbDepartment);
        System.out.println("-------------------------------------------------------------");
        //Muestra todos los empleados que pertecenen a un departamento de LPGC quivale a un Join en MySQL
        showAllEmployed(dbEmployed);
        System.out.println("-------------------------------------------------------------");
        //Editar un empleado cambiandole el nombre, en mi caso solo cambie el nombre del primer empleado de la base de datos
        EditEmployed(dbEmployed);
        System.out.println("-------------------------------------------------------------");
        //Borrar los empleado que tienen un sueldo menor a 1000
        DeleteEmployed(dbEmployed);

        dbDepartment.close();
        dbEmployed.close();
    }

    private static void DeleteEmployed(ObjectContainer dbEmployed) {
        ObjectSet<Empleados> EmployedList=dbEmployed.queryByExample(new Empleados(null, null, 0 , null));
        while (EmployedList.hasNext()){
            Empleados emp=EmployedList.next();
            if (emp.getSalario()<1000){
                dbEmployed.delete(emp);
                System.out.println("Se ha eliminado el empleado: "+emp.getNombre());
            }
        }
    }

    private static void EditEmployed(ObjectContainer dbEmployed) {
        ObjectSet<Empleados> EmployedList=dbEmployed.queryByExample(new Empleados(null, null, 0 , null));
        Empleados emp=EmployedList.next();
        emp.setNombre("Luisa");
        dbEmployed.store(emp);

        showAllEmployed(dbEmployed);
    }

    private static void showAllEmployed(ObjectContainer dbEmployed) {
        ObjectSet<Empleados> EmployedList=dbEmployed.queryByExample(new Empleados(null, null, 0,new Departamentos(0, null,"LPGC")));
        while (EmployedList.hasNext()){
            Empleados emp=EmployedList.next();
            System.out.println(emp.getDni()+ " | "+emp.getNombre()+" | "+emp.getSalario()+" | "+emp.getDepartamentos().getNombre()+" | "+emp.getDepartamentos().getLocalidad());
        }
    }

    private static void showAllDeparments(ObjectContainer dbDepartment) {
        ObjectSet<Departamentos> DepartmentList=dbDepartment.queryByExample(new Departamentos(0, null, null));
        while (DepartmentList.hasNext()){
            Departamentos dept=DepartmentList.next();
            System.out.println(dept.getDeptno()+ " | "+dept.getNombre()+" | "+dept.getLocalidad());
        }
    }

    private static void db4Connection(ObjectContainer dbDepartment, ObjectContainer dbEmployed) {
        try {

            if (!ControlPK.existeObjetoDepatamento(30, dbDepartment) && !ControlPK.existeObjetoEmpleado("345k", dbEmployed) && !ControlPK.existeObjetoEmpleado("346l", dbEmployed)) {

                Departamentos departamento = new Departamentos(30, "Comercial", "LPGC");
                System.out.println("Departamento creado");

                Empleados empleado1 = new Empleados("346l", "Pepe", 2000 ,departamento);
                System.out.println("Empleado " + empleado1.getNombre() + " insertado");
                Empleados empleado2 = new Empleados("345k", "Marta", 1000 ,departamento);
                System.out.println("Empleado " + empleado2.getNombre() + " insertado");

                dbDepartment.store(departamento);
                dbEmployed.store(empleado1);
                dbEmployed.store(empleado2);
            } else {
                throw new Db4oException("Los datos que quieres insertar coinciden con los que estan en la base de datos");
            }
            if (!ControlPK.existeObjetoDepatamento(25, dbDepartment) && !ControlPK.existeObjetoEmpleado("400k", dbEmployed) && !ControlPK.existeObjetoEmpleado("401l", dbEmployed)) {
                Departamentos departamento = new Departamentos(25, "Ventas", "LPGC");
                System.out.println("Departamento creado");

                Empleados empleado1 = new Empleados("400k", "Marcos", 850 ,departamento);
                System.out.println("Empleado " + empleado1.getNombre() + " insertado");
                Empleados empleado2 = new Empleados("401l", "Juan", 944,departamento);
                System.out.println("Empleado " + empleado2.getNombre() + " insertado");

                dbDepartment.store(departamento);
                dbEmployed.store(empleado1);
                dbEmployed.store(empleado2);
            } else {
                throw new Db4oException("Los datos que quieres insertar coinciden con los que estan en la base de datos");
            }

            if (!ControlPK.existeObjetoDepatamento(15, dbDepartment) && !ControlPK.existeObjetoEmpleado("402k", dbEmployed) && !ControlPK.existeObjetoEmpleado("403l", dbEmployed)) {
                Departamentos departamento = new Departamentos(15, "Desarrollo", "SCT");
                System.out.println("Departamento creado");

                Empleados empleado1 = new Empleados("402k", "Yasmina", 1455 ,departamento);
                System.out.println("Empleado " + empleado1.getNombre() + " insertado");
                Empleados empleado2 = new Empleados("403l", "Maria",  1455,departamento);
                System.out.println("Empleado " + empleado2.getNombre() + " insertado");

                dbDepartment.store(departamento);
                dbEmployed.store(empleado1);
                dbEmployed.store(empleado2);
            } else {
                throw new Db4oException("Los datos que quieres insertar coinciden con los que estan en la base de datos");
            }

        } catch (Db4oException e) {
            e.printStackTrace();
        }
    }
}
