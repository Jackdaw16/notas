public class Db4oException extends Exception {
    public Db4oException(String msg){
        super(msg);
    }
}
