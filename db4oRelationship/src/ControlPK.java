import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class ControlPK {
    public static boolean existeObjetoDepatamento(int cod, ObjectContainer db_dep) {

        ObjectSet<Departamentos> var = db_dep.queryByExample(new Departamentos(cod,null,null));
        if (var.size() > 0) {
            return true;
        }

        return false;
    }

    public static boolean existeObjetoEmpleado(String dni, ObjectContainer db_emp){
        ObjectSet<Empleados> var= db_emp.queryByExample(new Empleados(dni, null, 0, null));
        if (var.size()>0){
            return true;
        }

        return false;
    }
}
