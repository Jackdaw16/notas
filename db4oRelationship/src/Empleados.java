public class Empleados {
    private String dni;
    private String nombre;
    private double salario;
    Departamentos departamentos;


    public Empleados(String dni, String nombre, double salario, Departamentos departamentos) {
        this.dni = dni;
        this.nombre = nombre;
        this.salario = salario;
        this.departamentos = departamentos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Departamentos getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(Departamentos departamentos) {
        this.departamentos = departamentos;
    }
}
