import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class Main {
    public static void main(String[] args) {
        ObjectContainer db= Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), "dbPersonas.yap");

        //Personas persona1=new Personas("345g", "Pepe", 22);
        //Personas persona2=new Personas("546j", "Luisa", 18);

        //db.store(persona1);
        //db.store(persona2);

        //Busquedas

        ObjectSet<Personas> ListaPersonas=db.queryByExample(new Personas(null, null, 0)); //select * from Personas
        while (ListaPersonas.hasNext()){
            Personas per=ListaPersonas.next();
            System.out.println(per.getNif()+" "+per.getName()+" "+per.getAge());
        }

        ListaPersonas=db.queryByExample(new Personas(null, "Luisa", 0)); //select * from Personas where (Personas.name like 'Luisa')
        while (ListaPersonas.hasNext()){
            Personas per2=ListaPersonas.next();
            System.out.println(per2.getNif()+ " "+ per2.getName()+ " "+per2.getAge());
        }

        //Modificaciones
        ListaPersonas=db.queryByExample(new Personas(null, "Luisa", 0));
        while (ListaPersonas.hasNext()){
            Personas per3=ListaPersonas.next();
            per3.setAge(19);
            db.store(per3);
        }

        ListaPersonas=db.queryByExample(new Personas(null, "Luisa", 0)); //select * from Personas where (Personas.name like 'Luisa')
        while (ListaPersonas.hasNext()){
            Personas per2=ListaPersonas.next();
            System.out.println(per2.getNif()+ " "+ per2.getName()+ " "+per2.getAge());
        }

        //Eliminaciones
        ListaPersonas=db.queryByExample(new Personas(null, "Pepe", 0));
        while (ListaPersonas.hasNext()){
            Personas per4=ListaPersonas.next();
            db.delete(per4);
        }

        ListaPersonas=db.queryByExample(new Personas(null, null, 0)); //select * from Personas
        while (ListaPersonas.hasNext()){
            Personas per=ListaPersonas.next();
            System.out.println(per.getNif()+" "+per.getName()+" "+per.getAge());
        }


        db.close();
    }
}
